package com.qfedu.utils;

import com.qfedu.common.JsonResult;

public class JsonUtils {

    public static JsonResult createJsonBean(int code, Object info){
        JsonResult result = new JsonResult();
        result.setCode(code);
        result.setInfo(info);
        return result;
    }
}
