package com.qfedu.controller;

import com.qfedu.common.JsonResult;
import com.qfedu.service.MailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.UnsupportedEncodingException;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/9/1
 * Time: 14:07
 * Code introduction:
 */

@Controller
@RequestMapping("/mail")
public class MailController {
    @Autowired
    private MailService mailService;
    @RequestMapping("/sendMail.do")
    @ResponseBody
    public JsonResult sendMail(String recipient, String title, String content){

        System.out.println(recipient+"----"+title+"----"+content);

        JsonResult jsonResult;
        try {
            mailService.SendMail(recipient,title,content);
            jsonResult = new JsonResult(1,"发送成功");

        } catch (Exception e) {
            jsonResult = new JsonResult(0,"发送失败");
            e.printStackTrace();
        }

        return jsonResult;
    }

}
