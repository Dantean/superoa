package com.qfedu.controller;

import com.github.pagehelper.Page;
import com.qfedu.common.JsonResult;
import com.qfedu.entity.Dept;
import com.qfedu.service.DeptService;
import com.qfedu.vo.DeptEcharts;
import com.qfedu.vo.DeptVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/after/dept")
public class DeptController {
    @Autowired
    private DeptService deptService;

//  查询部门
    @RequestMapping("/selectDept.do")
    @ResponseBody
    public Map<String,Object> selectDept(String dname, Integer page, Integer limit) {
//        User user = (User)session.getAttribute(StrUtil.LOGIN_USER);
//
        List<DeptVO> list = deptService.findAll(dname,page,limit);
//        List<Dept> list = deptService.findD(page,limit);
        HashMap<String, Object> map = new HashMap<>();

//
        //通过Page对象的getTotal方法获取数据总数量
        long total = ((Page) list).getTotal();

//        不能用list.size()----获取的是一页数据量
//        int total = list.size();

        map.put("code",0);
        map.put("msg","");
        map.put("count",total);
        map.put("data",list);

        return map;
    }

//单行删除
    @RequestMapping("/deleteDeptById.do")
    @ResponseBody
    public JsonResult deleteDeptById(String did) {

        deptService.deleteDeptById(did);
        return new JsonResult(1,"删除成功");

    }


//    批量删除
    @RequestMapping("/deleteDeptByIds.do")
    @ResponseBody
    public JsonResult deleteDeptByIds(String dids) {

        System.out.println("删除" + dids);
        deptService.deleteDeptByIds(dids);
        return new JsonResult(1,"批量删除成功");

    }


    //    添加部门
    @RequestMapping("/addDept.do")
    @ResponseBody
    public JsonResult addDept(Dept dept) {


        deptService.addDepts(dept);
        return new JsonResult(1,"添加成功");

    }



    //    修改部门
    @RequestMapping("/updateDept.do")
    @ResponseBody
    public JsonResult updateDept(Dept dept) {

        System.out.println(dept+ "==============");
        deptService.updateDeptInfo(dept);
        return new JsonResult(1,"修改成功");

    }


    //   Echarts展示
    @RequestMapping("/showEcharts.do")
    @ResponseBody
    public DeptEcharts showEcharts() {

        DeptEcharts deptEcharts = deptService.selectDeptByEcharts();
        return deptEcharts;

    }

    //文件下载 数据导出
    @RequestMapping("/downloadExcel.do")
    public void down(HttpServletResponse response) throws IOException {
        deptService.download(response);
    }

//    根据雇员id查询部门信息
    @RequestMapping("/selectDeptByEid.do")
    @ResponseBody
    public  JsonResult selectDeptByEid(Integer id) {
        Dept dept = deptService.selectDeptByeid(id);

        return new JsonResult(1,dept);


}
}
