package com.qfedu.controller;

import com.github.pagehelper.Page;
import com.qfedu.common.JsonResult;
import com.qfedu.entity.Reimbursement;
import com.qfedu.service.ReimbursementService;
import com.qfedu.vo.ReimbursementVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/after/finance")
public class ReimbursementController {
    @Autowired
    private ReimbursementService reimbursementService;

    //  查询所有报销记录
    @RequestMapping("/selectReimbursement.do")
    @ResponseBody
    public Map<String,Object> selectReimbursement(String name, Date beginTime , Date endTime, Integer page, Integer limit) {


        List<ReimbursementVo> list =reimbursementService.selectAllReimbursementInfo(name,beginTime,endTime,page, limit);
        HashMap<String,Object> map = new HashMap<>();

        long total = ((Page) list).getTotal();

        map.put("code",0);
        map.put("msg","");
        map.put("count",total);
        map.put("data",list);

        return map;
    }



    //单行删除
    @RequestMapping("/deleteReimbursementById.do")
    @ResponseBody
    public JsonResult deleteReimbursementById(String id) {


        return new JsonResult(1,"删除成功");

    }


    //    批量删除
    @RequestMapping("/deleteReimbursementByIds.do")
    @ResponseBody
    public JsonResult deleteReimbursementByIds(String aids) {


        return new JsonResult(1,"批量删除成功");

    }


    //    添加报销信息
    @RequestMapping("/addReimbursement.do")
    @ResponseBody
    public JsonResult addReimbursement(Reimbursement reimbursement) {

        reimbursementService.addReimbursementInfo(reimbursement);
        return new JsonResult(1,"添加成功");

    }


}
