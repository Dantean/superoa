package com.qfedu.controller;

import com.github.pagehelper.Page;
import com.qfedu.common.JsonResult;
import com.qfedu.entity.AskLeave;
import com.qfedu.service.AskLeaveService;
import com.qfedu.vo.AskLeaveVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/askLeave")
public class AskLeaveController {

    @Autowired
    private AskLeaveService askLeaveService;

    /**
     * @description: 根据条件查询符合的请假记录，返回Json数据
     * @param askLeave: 起始时间段内有请假时间重合的记录，申请时间为同一天的，其余都为精确匹配
     * @return: com.qfedu.common.JsonResult
     * @author: dazer
     * @date: 2020/9/6 16:14
     */
    @RequestMapping("/select.do")
    @ResponseBody
    public Map<String, Object> select(AskLeave askLeave, Integer page, Integer limit) {

        Map<String, Object> map = new HashMap<>();
        List<AskLeaveVO> select = askLeaveService.select(askLeave, page, limit);
        // 获取总记录数
        long total = ((Page) select).getTotal();

        map.put("code", 0);
        map.put("msg", "this is a askLeave list");
        map.put("count", total);
        map.put("data", select);
        return map;
    }

    @RequestMapping("/add.do")
    @ResponseBody
    public JsonResult add(AskLeave askLeave) {
        int add = askLeaveService.add(askLeave);
        return new JsonResult(1, "添加成功");
    }

    @RequestMapping("/delete.do")
    @ResponseBody
    public JsonResult delete(AskLeave askLeave) {
        int delete = askLeaveService.delete(askLeave);
        return new JsonResult(1, "删除成功");
    }

    @RequestMapping("/deleteByIds.do")
    @ResponseBody
    public JsonResult deleteByIds(String ids) {
        int delete = askLeaveService.deleteByIds(ids);
        return new JsonResult(1, "删除成功");
    }

    @RequestMapping("/update.do")
    @ResponseBody
    public JsonResult update(AskLeave askLeave) {
        int update = askLeaveService.update(askLeave);
        return new JsonResult(1, "修改成功");
    }

    @RequestMapping("/selectAllType.do")
    @ResponseBody
    public JsonResult selectAllStatus() {
        List<AskLeaveVO> askLeaveVOS = askLeaveService.selectAllType();
        return new JsonResult(1, askLeaveVOS);
    }

}
