package com.qfedu.controller;


import com.github.pagehelper.Page;
import com.qfedu.service.LogService;
import com.qfedu.vo.LogVO;
import com.qfedu.vo.LoginLogVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@ResponseBody
@RequestMapping("/log")
public class LogController {

    @Resource
    private LogService logService;

    @RequestMapping("/selectAll.do")
    public Map<String, Object> selectLog(LogVO logVO, Integer page, Integer limit) {

        List<LogVO> list = logService.selectLog(logVO, page, limit);

        // 获取数据库总条数
        long total = ((Page) list).getTotal();
        HashMap<String, Object> map = new HashMap<>();
        map.put("code",0);
        map.put("msg","");
        map.put("count",total);
        map.put("data",list);
        return map;


    }

    @RequestMapping("/selectLoginLog.do")
    public Map<String, Object> selectLoginLog(Date beginTime, Date endTime, String loginName,Integer page, Integer limit) {

        List<LoginLogVO> list = logService.selectLoginLog(beginTime, endTime, loginName, page, limit);

        // 获取数据库总条数
        long total = ((Page) list).getTotal();
        HashMap<String, Object> map = new HashMap<>();
        map.put("code",0);
        map.put("msg","");
        map.put("count",total);
        map.put("data",list);
        return map;


    }
}
