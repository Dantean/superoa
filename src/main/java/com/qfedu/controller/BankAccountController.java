package com.qfedu.controller;

import com.github.pagehelper.Page;
import com.qfedu.common.JsonResult;
import com.qfedu.entity.BankAccount;
import com.qfedu.service.BankAccountService;
import com.qfedu.vo.BankAccountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/after/finance")
public class BankAccountController {
    @Autowired
    private BankAccountService bankAccountService;

    //  查询所有帐户
    @RequestMapping("/selectAccount.do")
    @ResponseBody
    public Map<String,Object> selectAccount(String name,Date beginTime , Date endTime, Integer page, Integer limit) {


        List<BankAccountVo> list = bankAccountService.findAll(name,beginTime,endTime,page, limit);
        HashMap<String,Object> map = new HashMap<>();

        long total = ((Page) list).getTotal();

        map.put("code",0);
        map.put("msg","");
        map.put("count",total);
        map.put("data",list);

        return map;
    }


    
    //    添加账户
    @RequestMapping("/addAccount.do")
    @ResponseBody
    public JsonResult addAccount(BankAccount bankAccount) {

        bankAccountService.addBankAccount(bankAccount);
        return new JsonResult(1,"添加成功");

    }


    //单行删除
    @RequestMapping("/deleteAccountById.do")
    @ResponseBody
    public JsonResult deleteAccountById(String id) {

        bankAccountService.deleteBankAccountById(id);
        return new JsonResult(1,"删除成功");

    }


    //    批量删除
    @RequestMapping("/deleteAccountByIds.do")
    @ResponseBody
    public JsonResult deleteAccountByIds(String aids) {

        bankAccountService.deleteBankAccountByIds(aids);
        return new JsonResult(1,"批量删除成功");

    }


    //    修改账户
    @RequestMapping("/updateAccount.do")
    @ResponseBody
    public JsonResult updateAccount(BankAccount bankAccount) {
        System.out.println("测试" + bankAccount);

        bankAccountService.updateBankAccount(bankAccount);
        return new JsonResult(1,"修改成功");

    }

}
