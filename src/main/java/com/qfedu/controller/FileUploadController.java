package com.qfedu.controller;


import com.qfedu.service.FileUploadService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/file")
public class FileUploadController {

    @Autowired
    private FileUploadService fus;
    // 文件上传
    @ResponseBody
    @RequestMapping("/batch.do")
    public int batch(MultipartFile file) throws IOException {
        return fus.batch(file);
    }

   // 文件下载
   @RequestMapping("/down.do")
   public void down(Integer num, HttpServletResponse response) throws IOException {
        fus.download(num,response);
   }

}
