package com.qfedu.controller;


import com.qfedu.common.JsonResult;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;

@Controller
@ResponseBody
public class OnLineCountController {

    @RequestMapping("/count.do")
    public JsonResult onLineCount(HttpServletRequest request) {
        ServletContext servletContext = request.getServletContext();
        Object count = servletContext.getAttribute("Count");
        if (count != null) {
            count = Integer.parseInt(count.toString());
        }
        System.out.println("在线人数" + count);
        return new JsonResult(1, count);

    }
}
