package com.qfedu.controller;

import com.github.pagehelper.Page;
import com.qfedu.common.JsonResult;
import com.qfedu.entity.User;
import com.qfedu.service.UserService;
import com.qfedu.utils.JsonUtils;
import com.qfedu.utils.StrUtils;
import com.qfedu.vo.UserVO;
import com.sun.scenario.effect.impl.sw.sse.SSEBlend_SRC_OUTPeer;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;


import javax.imageio.ImageIO;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.awt.*;
import java.awt.image.BufferedImage;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    UserService userService;

    //登录
    @RequestMapping("/login.do")
    @ResponseBody
    public JsonResult login(String loginname, String password, String imageCode, HttpSession session) {
        JsonResult result = null;

        String checkCode_session = (String) session.getAttribute("checkCode_session");

        try {

            if (checkCode_session.equalsIgnoreCase(imageCode)) {
//            忽略大小写比较是否相同

                User users = userService.findByName(loginname, password);
                Integer id = users.getId();
                userService.updateUser1(id,1);
                session.setAttribute(StrUtils.LOGIN_USER, users);
                result = JsonUtils.createJsonBean(1, users);
            } else {
                result = JsonUtils.createJsonBean(0, "信息有误");
            }
        } catch (Exception e) {
            result = JsonUtils.createJsonBean(0, e.getMessage());
        }
        return result;
    }

    //    添加用户
    @ResponseBody
    @RequestMapping("/addUser.do")
    public JsonResult addUser(User user) {
        JsonResult result = null;

        try {
            userService.addUser(user);
            result = JsonUtils.createJsonBean(1, "添加成功");
        } catch (Exception e) {
            result = JsonUtils.createJsonBean(0, "添加失败");
        }
        return result;
    }

    //    删除用户
    @ResponseBody
    @RequestMapping("/deleteUser.do")
    public JsonResult deleteUser(String ids) {
        JsonResult result = null;
        userService.deleteUser(ids);
        result = JsonUtils.createJsonBean(1, "删除成功");
        return result;
    }

    //    修改用户
    @ResponseBody
    @RequestMapping("/updateUser.do")
    public JsonResult updateUser(User user) {

        JsonResult result = null;

        try {
            userService.updateUser(user);
            result = JsonUtils.createJsonBean(1, "修改成功");
        } catch (Exception e) {
            result = JsonUtils.createJsonBean(0, "修改失败");
        }
        return result;
    }

    //    查找用户
    @ResponseBody
    @RequestMapping("/selectUser.do")
    public Map<String, Object> selectUser(Integer page, Integer limit) {

        List<User> list = userService.selectUser(page, limit);

        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "");
        map.put("count", ((Page) list).getTotal());
        map.put("data", list);
        return map;
    }

    //    查找一个
    @ResponseBody
    @RequestMapping("/selectUserById.do")
    public JsonResult selectUserById(Integer id) {
        JsonResult result = null;

        User user = userService.selectUserById(id);
        result = new JsonResult(1, user);
        return result;
    }

    //    登录验证码
    @RequestMapping("/imageLogin.do")
    public void imageLogin(HttpSession session, HttpServletResponse response) {

        try {

            int width = 100;
            int height = 50;
//        创建一个验证码图片对象
            BufferedImage image = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);

//        美化图片
            Graphics g = image.getGraphics();   // 画笔对象
            g.setColor(Color.PINK);     //设置画笔颜色
            g.fillRect(0, 0, width, height);
//        图片边框
            g.setColor(Color.BLUE);
            g.drawRect(0, 0, width - 1, height - 1);

//        写验证码
            String str = "123456789";
//        生成随机角标
            Random ran = new Random();

            StringBuilder sb = new StringBuilder();

            for (int i = 1; i <= 4; i++) {
                int index = ran.nextInt(str.length());
//            获取字符
                char ch = str.charAt(index);

                sb.append(ch);
//            写验证码
                g.drawString(ch + "", width / 5 * i, height / 2);
            }

            String checkCode_session = sb.toString();
//            验证码存入session
            session.setAttribute("checkCode_session", checkCode_session);

//        画干扰线
            g.setColor(Color.GREEN);
//        随机生成坐标点
            for (int i = 0; i < 10; i++) {
                int x1 = ran.nextInt(width);
                int x2 = ran.nextInt(width);

                int y1 = ran.nextInt(height);
                int y2 = ran.nextInt(height);
                g.drawLine(x1, y1, x2, y2);
            }

            ImageIO.write(image, "jpg", response.getOutputStream());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //    返回给前端图表

    @ResponseBody
    @RequestMapping("/batch.do")
    public JsonResult batch(@RequestParam MultipartFile file, HttpSession session) throws IOException {

        User user = (User) session.getAttribute(StrUtils.LOGIN_USER);


        String s = userService.upUserAvatar(file, user);

        return new JsonResult(1,s);
    }
    @ResponseBody
    @RequestMapping("/userChart.do")
    public UserVO userChart() {
        return userService.userChart();
    }

    @ResponseBody
    @RequestMapping("/queryName.do")
    public JsonResult queryName(HttpSession session) {
        User user = (User)session.getAttribute(StrUtils.LOGIN_USER);
        return new JsonResult(1,user);
    }

    @RequestMapping("/register.do")
    @ResponseBody
    public JsonResult register(String loginname, String username,String password, String queryPassword) {
        userService.addOneUser(loginname,username,password,queryPassword);
        return new JsonResult(1, "亲，注册成功，请登录哦");
    }

}
