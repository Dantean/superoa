package com.qfedu.controller;



import com.github.pagehelper.Page;
import com.qfedu.common.JsonResult;
import com.qfedu.dto.ViewDateObject;
import com.qfedu.entity.User;
import com.qfedu.service.AdviceService;
import com.qfedu.utils.StrUtils;
import com.qfedu.vo.AdviceEcharts;
import com.qfedu.vo.AdviceVO;

import com.qfedu.common.JsonResult;
import com.qfedu.entity.Advice;
import com.qfedu.entity.User;
import com.qfedu.service.AdviceService;
import com.qfedu.utils.StrUtils;
import com.qfedu.vo.EchartsVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

@Controller  // 相当于实现Controller接口
@RequestMapping("/advices")  // 给url统一添加前缀
@ResponseBody   // 设置返回数据都为json字符串形式  我喜欢你ma
public class AdviceController {

    @Autowired
    private AdviceService adviceService;

    @RequestMapping("/selectAdvices.do")
    public Map<String, Object> selectByUid(HttpSession session, String title, String content, Integer page, Integer limit) {
        User user = (User) session.getAttribute(StrUtils.LOGIN_USER);
        List<AdviceVO> list = adviceService.selectByUid(user, title, content, page, limit);
        // 获取总数据条数
        long total = ((Page) list).getTotal();
        HashMap<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "");
        map.put("count", total);
        map.put("data", list);
        return map;
    }

    @RequestMapping("/delete.do")
    public JsonResult deleteById(HttpSession session, Integer id) {
        System.out.println(id);
        User user = (User) session.getAttribute(StrUtils.LOGIN_USER);
        adviceService.deleteById(user, id);
        return new JsonResult(1, "删除成功");
    }

    @RequestMapping("/deletes.do")
    public JsonResult deleteByIds(HttpSession session, String ids) {
        User user = (User) session.getAttribute(StrUtils.LOGIN_USER);
        adviceService.deleteByIds(user, ids);
        return new JsonResult(1, "批量删除成功");
    }


    /**
     * @param session 通过StrUtils.LOGIN_USER获取当前登录用户的id
     * @param title   公告标题
     * @param content 公共内容
     * @return 返回给前端ajax是否添加成功的json字符串
     */
    @RequestMapping("/addAdvice.do")
    public JsonResult addAdvice(HttpSession session, String title, String content) {
        System.out.println(title + "------------" + content);

        JsonResult jsonResult;
        User user = (User) session.getAttribute(StrUtils.LOGIN_USER);
        if (user != null) {
            jsonResult = new JsonResult(0, "请登录");
        } else {
            Advice addAdv = new Advice();
            addAdv.setUid(1);
            addAdv.setContent(content);
            addAdv.setTitle(title);

            adviceService.addAdvice(addAdv);

            Integer id = addAdv.getId();
            System.out.println(id);
            if (id != 0) {
                jsonResult = new JsonResult(1, "插入成功");
            } else {
                jsonResult = new JsonResult(0, "插入失败");
            }
        }

        return jsonResult;
    }


    @RequestMapping("/selectById.do")
    public JsonResult selectById(HttpSession session, Integer id) {
        User user = (User)session.getAttribute(StrUtils.LOGIN_USER);
        Advice advice = adviceService.selectById(user, id);
        return new JsonResult(1,advice);

    }

    @RequestMapping("/updateAdvice.do")
    public JsonResult updateAdvice(HttpSession session, Advice advice) {


        User user = (User) session.getAttribute(StrUtils.LOGIN_USER);

        adviceService.updateAdvice(user,advice);

        return new JsonResult(1,"修改成功");
    }
    @RequestMapping("/echarts.do")
    public JsonResult echarts(){
        JsonResult jsonResult;
        EchartsVO echarts = adviceService.echarts();
        return new JsonResult(1,echarts);
    }
    @RequestMapping("/echartsAdmin.do")
    public JsonResult echartsAdmin(){
        JsonResult jsonResult;
        List<EchartsVO> echartsVOS = adviceService.echartsAdmin();
        return new JsonResult(1,echartsVOS);
    }



    @RequestMapping("/echarts1.do")
    public AdviceEcharts selectGroupBy() {
        return adviceService.selectGroupBy();
    }

}

