package com.qfedu.controller;

import com.qfedu.common.JsonResult;
import com.qfedu.dao.SignDao;
import com.qfedu.entity.Sign;
import com.qfedu.entity.User;
import com.qfedu.service.SignService;
import com.qfedu.utils.JsonUtils;
import com.qfedu.utils.StrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpSession;

@Controller
@RequestMapping("/sign")
public class SignController {

    @Autowired
    SignService signService;

    @ResponseBody
    @RequestMapping("/moSign.do")
    public JsonResult moSign(Sign sign, HttpSession session) {
        User user = (User) session.getAttribute(StrUtils.LOGIN_USER);
        sign.setUid(user.getId());
        String flag = signService.moSign(sign);
        return new JsonResult(1, flag);
    }
}
