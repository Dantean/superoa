package com.qfedu.controller;

import com.qfedu.common.JsonResult;
import com.qfedu.entity.Employee;
import com.qfedu.service.EmployeeService;
import com.github.pagehelper.Page;
import com.qfedu.vo.EmployeeVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping("/add.do")
    @ResponseBody
    public JsonResult add( Employee employee) {
        employeeService.addEmployee(employee);
        return new JsonResult(1, "添加成功");
    }

    @RequestMapping("/update.do")
    @ResponseBody
    public JsonResult update(Employee employee) {
        employeeService.update(employee);
        return new JsonResult(1,"执行修改操作");
    }

    @RequestMapping("/selectFuzzy.do")
    @ResponseBody
    public Map<String, Object> selectFuzzy(Employee employee, Integer page, Integer limit) {
        Map<String, Object> map = new HashMap<>();
        List<EmployeeVO> employeeVOList = employeeService.selectFuzzy(employee, page, limit);

        // 获取总记录数
        long total = ((Page) employeeVOList).getTotal();

        map.put("code", 0);
        map.put("msg", "this is a employee list");
        map.put("count", total);
        map.put("data", employeeVOList);
        return map;
    }


    /**
     * 根据前端传的 id 字符串，删除对应的一些员工
     * @param ids 员工的 id 字符串，中间用 逗号','  间隔
     * @return 返回 Json数据，状态码code为 1 表示删除成功
     */
    @RequestMapping("/delete.do")
    @ResponseBody
    public JsonResult delete(String ids) {
        // ids = "7, 9";
        employeeService.deleteByIds(ids);
        return new JsonResult(1, "删除成功");
    }

}
