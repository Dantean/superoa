package com.qfedu.controller;

import com.github.pagehelper.Page;
import com.qfedu.common.JsonResult;
import com.qfedu.entity.Rent;
import com.qfedu.entity.RentGoods;
import com.qfedu.service.RentService;
import com.qfedu.utils.JsonUtils;
import com.qfedu.vo.RentGoodsVO;
import com.qfedu.vo.RentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping("/rent")
public class RentController {

    @Autowired
    private RentService rentService;

    @ResponseBody
    @RequestMapping("/selectRentGoods.do")
    public Map<String, Object> SelectRentGoods(Integer page, Integer limit) {

        List<RentGoodsVO> list = rentService.selectRentGoods(page, limit);

        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "");
        map.put("count", ((Page) list).getTotal());
        map.put("data", list);

        System.out.println(map.toString());
        return map;
    }

    @ResponseBody
    @RequestMapping("/addRentGood.do")
    public JsonResult addRentGood(RentGoods rentGoods) {

        rentService.addRentGood(rentGoods);
        return JsonUtils.createJsonBean(1, "添加成功");
    }

    @ResponseBody
    @RequestMapping("/selectRentGoodById.do")
    public JsonResult selectRentGoodById(Integer id) {
        return JsonUtils.createJsonBean(1, rentService.selectRentGoodById(id));
    }

    @ResponseBody
    @RequestMapping("/updateRentGood.do")
    public JsonResult updateRentGood(RentGoods rentGoods) {

        JsonResult result = null;
        try {
            rentService.updateRentGood(rentGoods);
            result = JsonUtils.createJsonBean(1, "修改成功");
        } catch (Exception e) {
            result = JsonUtils.createJsonBean(0, e.getMessage());
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("/deleteRentGoods.do")
    public JsonResult deleteRentGoods(String ids) {
        JsonResult result = null;

        try {
            rentService.deleteRentGoods(ids);
            result = JsonUtils.createJsonBean(1, "删除成功");
        } catch (Exception e) {
            result = JsonUtils.createJsonBean(0, e.getMessage());
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("/selectRent.do")
    public Map<String, Object> selectRent(Integer page, Integer limit) {
        List<RentVO> list = rentService.selectRent(page, limit);

        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "");
        map.put("count", ((Page) list).getTotal());
        map.put("data", list);
        return map;
    }

    @ResponseBody
    @RequestMapping("/addRent.do")
    public JsonResult addRent(Rent rent) {

        System.out.println(rent);

        JsonResult result = null;
        try {
            rentService.addRent(rent);
            result = JsonUtils.createJsonBean(1, "添加成功");
        } catch (Exception e) {
            result = JsonUtils.createJsonBean(0, "添加失败");
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("/updateRent.do")
    public JsonResult updateRent(Rent rent) {
        JsonResult result = null;
        try {
            rentService.updateRent(rent);
            result = JsonUtils.createJsonBean(1, "修改成功");
        } catch (Exception e) {
            e.printStackTrace();
            result = JsonUtils.createJsonBean(0, "修改失败");
        }
        return result;
    }

    @ResponseBody
    @RequestMapping("/selectRentById.do")
    public JsonResult selectRentById(Integer id) {
        JsonResult result = null;
        try {
            Rent rent = rentService.selectRentById(id);
            result = JsonUtils.createJsonBean(1, rent);
        } catch (Exception e) {
            result = JsonUtils.createJsonBean(0, e.getMessage());
        }

        return result;
    }
}