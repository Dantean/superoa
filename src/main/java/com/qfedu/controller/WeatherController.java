package com.qfedu.controller;

import com.qfedu.dto.WeatherEcharts;
import com.qfedu.dto.dayWeather;
import com.qfedu.service.DeptService;
import com.qfedu.utils.WeatherUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletRequest;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/9/5
 * Time: 15:27
 * Code introduction:
 */

@Controller
@RequestMapping("/weather")
public class WeatherController {
    @Autowired
    private DeptService deptService;
    @RequestMapping("/select.do")
    @ResponseBody
    public WeatherEcharts select(HttpServletRequest request) {

        WeatherEcharts weatherEcharts = WeatherUtils.selectWeaq("郑州");
        return weatherEcharts;
    }

    @RequestMapping("/selectqwe.do")
    @ResponseBody
    public  List<dayWeather> selectqwe(HttpServletRequest request) {
        List<dayWeather> dayList = WeatherUtils.selectWea("郑州");
        return dayList;

    }
}
