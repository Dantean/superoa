package com.qfedu.controller;

import com.qfedu.common.JsonResult;
import com.qfedu.service.AddressService;
import com.qfedu.vo.AddressVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/address")
public class AddressController {

    @Autowired
    private AddressService addressService;

    @RequestMapping("/province.do")
    @ResponseBody
    public JsonResult province() {
        List<AddressVO> addressVOS = addressService.selectProvinces();
        return new JsonResult(1, addressVOS);
    }

    @RequestMapping("/city.do")
    @ResponseBody
    public JsonResult city(String provinceCode) {
        List<AddressVO> addressVOS = addressService.selectCities(provinceCode);
        return new JsonResult(1, addressVOS);
    }

    @RequestMapping("/area.do")
    @ResponseBody
    public JsonResult area(String cityCode) {
        List<AddressVO> addressVOS = addressService.selectAreas(cityCode);
        return new JsonResult(1, addressVOS);
    }


}
