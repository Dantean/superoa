package com.qfedu.controller;
import com.qfedu.entity.User;
import com.qfedu.service.UserService;
import com.qfedu.utils.StrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@Controller
public class OutController {

    @Autowired
    UserService userService;

    @RequestMapping("/user/out.do")
    public void out(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // session若不存在 返回null
        HttpSession session = request.getSession(false);
        if (session != null) {
            User user = (User)session.getAttribute(StrUtils.LOGIN_USER);
            userService.updateUser1(user.getId(),0);
            // 销毁session
            session.invalidate();
        }
        // 重定向到登录页面
        response.sendRedirect(request.getContextPath() +"/after/user/login.html" );

    }

}
