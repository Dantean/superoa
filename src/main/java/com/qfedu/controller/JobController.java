package com.qfedu.controller;

import com.github.pagehelper.Page;
import com.qfedu.common.JsonResult;
import com.qfedu.entity.Job;
import com.qfedu.service.JobService;
import com.qfedu.vo.JobEcharts;
import com.qfedu.vo.JobInfoVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author HZF
 */
@Controller
@RequestMapping("/job")
public class JobController {
    @Resource
    private JobService jobService;

    /**
     * 展示数据到前端
     * @param leadName 模糊查询领导名字
     * @param position 模糊查询所属部门
     * @param page 页码
     * @param limit 每页限定条数
     * @return 查找到的数据
     */
    @RequestMapping("/selectJob.do")
    @ResponseBody
    public Map<String, Object> selectAllJob(String leadName, String position, Integer page, Integer limit) {

        List<JobInfoVO> list = jobService.selectAllJob(leadName, position, page, limit);

        /*获取总记录数*/
        long total = ((Page) list).getTotal();
        Map<String, Object> map = new HashMap<>();
        map.put("code", 0);
        map.put("msg", "");
        map.put("count", total);
        map.put("data", list);

        return map;
    }

    /**
     * 查询jobName用来渲染下拉框
     * @return 查找到的数据
     */
    @RequestMapping("/selectJobName.do")
    @ResponseBody
    public JsonResult selectJobName() {
        List<Job> list = jobService.selectJobName();
        return new JsonResult(1, list);
    }

    /**
     * 添加一个job类型
     * @param name job类型名字
     * @param remanrk job类型描述
     * @return 查找到的数据
     */
    @RequestMapping("/addJob.do")
    @ResponseBody
    public JsonResult addJobType(String name, String remanrk) {

        Job job = new Job();
        job.setName(name);
        job.setRemark(remanrk);

        jobService.addJobType(job);
        return new JsonResult(1, "成功");
    }

    /**
     * 删除一个job类型
     * @param id 通过employ表的id进行删除
     * @return 是否成功
     */
    @RequestMapping("/deleteJobInfo.do")
    @ResponseBody
    public JsonResult deleteJob(Integer id) {
        jobService.deleteJobById(id);
        return new JsonResult(1, "hello");
    }

    /**
     * 删除多个job类型
     * @param ids 通过employee表的id进行删除
     * @return 是否成功
     */
    @RequestMapping("/deleteLotsJob.do")
    @ResponseBody
    public JsonResult deleteLotsJob(String ids) {

        jobService.deleteLotsJob(ids);

        return new JsonResult(1, "删除成功");
    }

    /**
     * 展示job图表
     * @return 返回查找到的job数据表的job类型和人数
     */
    @RequestMapping("/showJobChart.do")
    @ResponseBody
    public JsonResult showJobChart() {

        JobEcharts jobEcharts = jobService.selectJobEchartsInfo();

        return new JsonResult(1, jobEcharts);
    }

    @RequestMapping("/loadJobChart.do")
    public void loadJobChart(HttpServletResponse response) {
        jobService.loadJobChart(response);
    }
}
