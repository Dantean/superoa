package com.qfedu.dto;

import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/9/5
 * Time: 16:47
 * Code introduction:
 */


public class dayWeather {
    private String jiangshui;//降水几率
    private String air_press;//气压
    private String weekday;//周几
    private String night_wind_direction;
    private String night_air_temperature;//夜间气温
    private String night_weather_pic;//
    private String night_weather_code;//
    private String night_weather;//
    private String day_weather_code;//
    private String ziwaixian;//紫外线
    private String day_weather;//
    private String day_wind_power;//
    private String day_weather_pic;//
    private String day_air_temperature;//白天气温
    private String day_wind_direction;//
    private String night_wind_power;//
    private String sun_begin_end;//日出日落时间
    private String day;//时间

    @Override
    public String toString() {
        return "dayWeather{" +
                "jiangshui='" + jiangshui + '\'' +
                ", air_press='" + air_press + '\'' +
                ", weekday='" + weekday + '\'' +
                ", night_wind_direction='" + night_wind_direction + '\'' +
                ", night_air_temperature='" + night_air_temperature + '\'' +
                ", night_weather_pic='" + night_weather_pic + '\'' +
                ", night_weather_code='" + night_weather_code + '\'' +
                ", night_weather='" + night_weather + '\'' +
                ", day_weather_code='" + day_weather_code + '\'' +
                ", ziwaixian='" + ziwaixian + '\'' +
                ", day_weather='" + day_weather + '\'' +
                ", day_wind_power='" + day_wind_power + '\'' +
                ", day_weather_pic='" + day_weather_pic + '\'' +
                ", day_air_temperature='" + day_air_temperature + '\'' +
                ", day_wind_direction='" + day_wind_direction + '\'' +
                ", night_wind_power='" + night_wind_power + '\'' +
                ", sun_begin_end='" + sun_begin_end + '\'' +
                ", day='" + day + '\'' +
                '}';
    }

    public String getJiangshui() {
        return jiangshui;
    }

    public void setJiangshui(String jiangshui) {
        this.jiangshui = jiangshui;
    }

    public String getAir_press() {
        return air_press;
    }

    public void setAir_press(String air_press) {
        this.air_press = air_press;
    }

    public String getWeekday() {
        return weekday;
    }

    public void setWeekday(String weekday) {
        this.weekday = weekday;
    }

    public String getNight_wind_direction() {
        return night_wind_direction;
    }

    public void setNight_wind_direction(String night_wind_direction) {
        this.night_wind_direction = night_wind_direction;
    }

    public String getNight_air_temperature() {
        return night_air_temperature;
    }

    public void setNight_air_temperature(String night_air_temperature) {
        this.night_air_temperature = night_air_temperature;
    }

    public String getNight_weather_pic() {
        return night_weather_pic;
    }

    public void setNight_weather_pic(String night_weather_pic) {
        this.night_weather_pic = night_weather_pic;
    }

    public String getNight_weather_code() {
        return night_weather_code;
    }

    public void setNight_weather_code(String night_weather_code) {
        this.night_weather_code = night_weather_code;
    }

    public String getNight_weather() {
        return night_weather;
    }

    public void setNight_weather(String night_weather) {
        this.night_weather = night_weather;
    }

    public String getDay_weather_code() {
        return day_weather_code;
    }

    public void setDay_weather_code(String day_weather_code) {
        this.day_weather_code = day_weather_code;
    }

    public String getZiwaixian() {
        return ziwaixian;
    }

    public void setZiwaixian(String ziwaixian) {
        this.ziwaixian = ziwaixian;
    }

    public String getDay_weather() {
        return day_weather;
    }

    public void setDay_weather(String day_weather) {
        this.day_weather = day_weather;
    }

    public String getDay_wind_power() {
        return day_wind_power;
    }

    public void setDay_wind_power(String day_wind_power) {
        this.day_wind_power = day_wind_power;
    }

    public String getDay_weather_pic() {
        return day_weather_pic;
    }

    public void setDay_weather_pic(String day_weather_pic) {
        this.day_weather_pic = day_weather_pic;
    }

    public String getDay_air_temperature() {
        return day_air_temperature;
    }

    public void setDay_air_temperature(String day_air_temperature) {
        this.day_air_temperature = day_air_temperature;
    }

    public String getDay_wind_direction() {
        return day_wind_direction;
    }

    public void setDay_wind_direction(String day_wind_direction) {
        this.day_wind_direction = day_wind_direction;
    }

    public String getNight_wind_power() {
        return night_wind_power;
    }

    public void setNight_wind_power(String night_wind_power) {
        this.night_wind_power = night_wind_power;
    }

    public String getSun_begin_end() {
        return sun_begin_end;
    }

    public void setSun_begin_end(String sun_begin_end) {
        this.sun_begin_end = sun_begin_end;
    }

    public String getDay() {
        return day;
    }

    public void setDay(String day) {
        this.day = day;
    }
}
