package com.qfedu.entity;

public class RentType {
    private Integer id;
    private String RentType;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getRentType() {
        return RentType;
    }

    public void setRentType(String rentType) {
        RentType = rentType;
    }
}