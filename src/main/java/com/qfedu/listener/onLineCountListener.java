package com.qfedu.listener;

import com.qfedu.utils.StrUtils;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.*;

@WebListener
public class onLineCountListener implements HttpSessionAttributeListener {
    @Override
    public void attributeAdded(HttpSessionBindingEvent event) {
        // 第一次登录为null
        Object count1 = event.getSession().getServletContext().getAttribute("Count");
        String name = event.getName();
        if (count1 != null) {
             int count = Integer.parseInt((count1).toString());
            System.out.println(count);
            if (name.equals(StrUtils.LOGIN_USER)) {
                count++;
                event.getSession().getServletContext().setAttribute("Count", count);
            }
        } else {
            int count = 0;
            if (name.equals(StrUtils.LOGIN_USER)) {
                count++;
                event.getSession().getServletContext().setAttribute("Count", count);
            }
        }
    }

    @Override
    public void attributeRemoved(HttpSessionBindingEvent event) {


    }

    @Override
    public void attributeReplaced(HttpSessionBindingEvent httpSessionBindingEvent) {

    }


}
