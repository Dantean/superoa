package com.qfedu.listener;


import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener
public class SessionListener implements HttpSessionListener {
    @Override
    public void sessionCreated(HttpSessionEvent httpSessionEvent) {

    }

    @Override
    public void sessionDestroyed(HttpSessionEvent event) {
        int count = (int)event.getSession().getServletContext().getAttribute("Count");
        if (count > 0) {
            count--;
        }
        event.getSession().getServletContext().setAttribute("Count",count);
        System.out.println("session已销毁");
    }
}
