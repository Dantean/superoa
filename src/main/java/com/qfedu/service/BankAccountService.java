package com.qfedu.service;



import com.qfedu.entity.BankAccount;
import com.qfedu.vo.BankAccountVo;

import java.util.Date;
import java.util.List;

public interface BankAccountService {
    List<BankAccountVo> findAll(String name, Date beginTime, Date endTime, Integer page, Integer limit);

    void addBankAccount(BankAccount bankAccount);

    void deleteBankAccountById(String id);

    void deleteBankAccountByIds(String id);

    void updateBankAccount(BankAccount bankAccount);
}
