package com.qfedu.service;

import com.qfedu.vo.LogVO;
import com.qfedu.vo.LoginLogVO;

import java.util.Date;
import java.util.List;

public interface LogService {
    List<LogVO> selectLog(LogVO logVO, Integer page, Integer limit);
    List<LoginLogVO> selectLoginLog(Date beginTime, Date endTime, String loginName, Integer page, Integer limit);
}
