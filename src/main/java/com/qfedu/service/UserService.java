package com.qfedu.service;

import com.qfedu.entity.User;
import com.qfedu.vo.UserVO;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;

public interface UserService {

    User findByName(String loginname,String password);

    void addUser(User user);

    void deleteUser(String ids);

    void updateUser(User user);

    void updateUser1(Integer id, Integer status);

    List<User> selectUser(Integer page, Integer limit);

    User selectUserById(Integer id);

    UserVO userChart();

    String upUserAvatar(MultipartFile file, User user) throws FileNotFoundException;

    void addOneUser(String loginname, String username, String password, String queryPassword);
}
