package com.qfedu.service;

import com.qfedu.entity.Rent;
import com.qfedu.entity.RentGoods;
import com.qfedu.vo.RentGoodsVO;
import com.qfedu.vo.RentVO;

import java.util.List;

public interface RentService {

    List<RentGoodsVO> selectRentGoods(Integer page, Integer limit);

    void addRentGood(RentGoods rentGoods);

    RentGoodsVO selectRentGoodById(Integer id);

    void updateRentGood(RentGoods rentGoods);

    void deleteRentGoods(String ids);

    List<RentVO> selectRent(Integer page, Integer limit);

    void addRent(Rent rent);

    Rent selectRentById(Integer id);

    void updateRent(Rent rent);

}
