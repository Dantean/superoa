package com.qfedu.service;

import com.qfedu.entity.Dept;
import com.qfedu.vo.DeptEcharts;
import com.qfedu.vo.DeptVO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public interface DeptService {
    List<DeptVO> findAll(String  dname,Integer page,Integer limit);

    void deleteDeptById(String did);

    void deleteDeptByIds(String did);

    void addDepts(Dept dept);

    void updateDeptInfo(Dept dept);

    DeptEcharts selectDeptByEcharts();

    void download(HttpServletResponse response) throws IOException;

    Dept selectDeptByeid(int eid);
}
