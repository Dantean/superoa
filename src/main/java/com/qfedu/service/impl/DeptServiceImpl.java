package com.qfedu.service.impl;

import com.alibaba.excel.EasyExcel;
import com.github.pagehelper.PageHelper;
import com.qfedu.dao.DeptDao;
import com.qfedu.entity.Dept;
import com.qfedu.service.DeptService;
import com.qfedu.vo.DeptEcharts;
import com.qfedu.vo.DeptVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Service
public class DeptServiceImpl implements DeptService {
    @Autowired
    private DeptDao deptDao;

    @Override
    public List<DeptVO> findAll(String dname, Integer page, Integer limit) {

        //设置页码和每页显示的记录数，该语句后面一定要紧跟着,
        // 他会在数据库查询相关语句会自动在SQL中传入limit ?,?
        //需要传入两个参数,第N页,每页M个
        PageHelper.startPage(page, limit);
        List<DeptVO> list = deptDao.selectDept(dname);
        return list;
    }

    @Override
    public void deleteDeptById(String did) {
        deptDao.deleteDeptById(did);
    }


    @Override
    public void deleteDeptByIds(String did) {

        String temp = did.replace("[","").replace("]","");
        String[] strs = temp.split(",");

        //将字符串数组转换成集合list
        List list = Arrays.asList(strs);
        

        System.out.println(list.size());
        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        deptDao.deleteDeptByIds(list);
    }

    @Override
    public void addDepts(Dept dept) {
        deptDao.addDept(dept);
    }


    @Override
    public void updateDeptInfo(Dept dept) {
        deptDao.updateDept(dept);
    }

    @Override
    public DeptEcharts selectDeptByEcharts() {
        List<DeptVO> list = deptDao.selectDeptEcharts();
        DeptEcharts deptEcharts = new DeptEcharts();
        deptEcharts.setDeptCount(new ArrayList<>());
        deptEcharts.setDeptName(new ArrayList<>());
        for (DeptVO deptVO : list) {
            deptEcharts.getDeptCount().add(deptVO.getCount());
            deptEcharts.getDeptName().add(deptVO.getDname());
        }
        return deptEcharts;
    }

    @Override
    public void download(HttpServletResponse response) throws IOException {
        //1.查询要生成的内容
        List<DeptVO> list=deptDao.selectDeptExcel();
        //2.设置下载 需要设置响应消息头
        //application/octet-stream 二进制 下载
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        //设置下载的文件名称
        response.setHeader("Content-Disposition","attachment;filename=Dept.xlsx");
        //将要下载的内容转换为Excel格式，并且写出
        EasyExcel.write(response.getOutputStream(),DeptVO.class).sheet("部门表"+list.size()).doWrite(list);
    }

    @Override
    public Dept selectDeptByeid(int eid) {
        return deptDao.selectDeptByEid(eid);
    }
}
