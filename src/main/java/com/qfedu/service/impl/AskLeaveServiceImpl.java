package com.qfedu.service.impl;

import com.github.pagehelper.PageHelper;
import com.qfedu.dao.AskLeaveDao;
import com.qfedu.entity.AskLeave;
import com.qfedu.service.AskLeaveService;
import com.qfedu.vo.AskLeaveVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AskLeaveServiceImpl implements AskLeaveService {

    @Autowired
    private AskLeaveDao askLeaveDao;

    @Override
    public int add(AskLeave askLeave) {
        int add = askLeaveDao.add(askLeave);
        if (add <= 0) {
            throw new RuntimeException("增加失败");
        }
        return add;
    }

    @Override
    public int update(AskLeave askLeave) {
        int update = askLeaveDao.update(askLeave);
        if (update <= 0) {
            throw new RuntimeException("修改失败");
        }
        return update;
    }

    @Override
    public List<AskLeaveVO> select(AskLeave askLeave, Integer page, Integer limit) {
        // 如果没有指定分页数据，则默认第一页，每页5条数据
        page = page == null ? 1 : page;
        limit = limit == null ? 5 : limit;

        PageHelper.startPage(page, limit);
        List<AskLeaveVO> select = askLeaveDao.select(askLeave);
        return select != null ? select : new ArrayList<>();
    }

    @Override
    public int delete(AskLeave askLeave) {
        int delete = askLeaveDao.delete(askLeave);
        if (delete <= 0) {
            throw new RuntimeException("删除失败");
        }
        return delete;
    }

    @Override
    public int deleteByIds(String ids) {
        int deleteByIds = askLeaveDao.deleteByIds(ids);
        if (deleteByIds <= 0) {
            throw new RuntimeException("批量删除失败");
        }
        return deleteByIds;
    }

    @Override
    public List<AskLeaveVO> selectAllType() {
        List<AskLeaveVO> askLeaveVOS = askLeaveDao.selectAllType();
        return askLeaveVOS != null ? askLeaveVOS : new ArrayList<>();
    }
}
