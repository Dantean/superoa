package com.qfedu.service.impl;


import com.qfedu.dao.EmployeeDao;
import com.qfedu.entity.Employee;
import com.qfedu.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.github.pagehelper.PageHelper;
import com.qfedu.vo.EmployeeVO;

import java.util.ArrayList;
import java.util.List;
/**
 * @author: Wufeng
 * @date: 2020/9/1 16:02
 * @description:
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeDao employeeDao;

    @Override
    public void addEmployee(Employee employee) {
        employeeDao.addEmployee(employee);
//        throw new RuntimeException("添加失败");
    }

    @Override
    public void update(Employee employee) {
        employeeDao.update(employee);
    }
    @Override
    public List<EmployeeVO> selectFuzzy(Employee employee, Integer page, Integer limit) {
        // 如果没有指定分页数据，则默认第一页，每页5条数据
        page = page == null ? 1 : page;
        limit = limit == null ? 5 : limit;

        PageHelper.startPage(page, limit);
        List<EmployeeVO> employeeVOList = employeeDao.selectFuzzy(employee);
        return employeeVOList == null ? new ArrayList<>() : employeeVOList;
    }

    @Override
    public int deleteByIds(String str) {
        return employeeDao.deleteByIds(str);
    }
}

