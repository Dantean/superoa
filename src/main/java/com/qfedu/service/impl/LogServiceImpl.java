package com.qfedu.service.impl;

import com.github.pagehelper.PageHelper;
import com.qfedu.dao.LogDao;
import com.qfedu.service.LogService;
import com.qfedu.vo.LogVO;
import com.qfedu.vo.LoginLogVO;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

@Component
public class LogServiceImpl implements LogService {
    @Resource
    private LogDao logDao;

    @Override
    public List<LogVO> selectLog(LogVO logVO, Integer page, Integer limit) {

        PageHelper.startPage(page,limit);
        return logDao.selectLog(logVO);
    }

    @Override
    public List<LoginLogVO> selectLoginLog(Date beginTime, Date endTime, String loginName, Integer page, Integer limit) {

        PageHelper.startPage(page,limit);
        return logDao.selectLoginLog(beginTime,endTime,loginName);
    }
}
