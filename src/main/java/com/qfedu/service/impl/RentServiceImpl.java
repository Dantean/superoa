package com.qfedu.service.impl;

import com.github.pagehelper.PageHelper;
import com.qfedu.dao.RentDao;
import com.qfedu.entity.Rent;
import com.qfedu.entity.RentGoods;
import com.qfedu.service.RentService;
import com.qfedu.vo.RentGoodsVO;
import com.qfedu.vo.RentVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class RentServiceImpl implements RentService {

    @Autowired
    private RentDao rentDao;

    @Override
    public List<RentGoodsVO> selectRentGoods(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        return rentDao.selectRentGoods();
    }

    @Override
    public void addRentGood(RentGoods rentGoods) {
        rentDao.addRentGood(rentGoods);
    }

    @Override
    public RentGoodsVO selectRentGoodById(Integer id) {
        return rentDao.selectRentGoodById(id);
    }

    @Override
    public void updateRentGood(RentGoods rentGoods) {
        rentDao.updateRentGood(rentGoods);
    }

    @Override
    public void deleteRentGoods(String ids) {
        ArrayList<Integer> list = new ArrayList<>();
        String[] target = ids.split(",");
        for (int i = 0; i < target.length; i++) {
            list.add(Integer.parseInt(target[i]));
        }
        rentDao.deleteRentGoods(list);
    }

    @Override
    public List<RentVO> selectRent(Integer page, Integer limit) {

        PageHelper.startPage(page, limit);

        List<RentVO> list = rentDao.selectRent();
        for (RentVO rentVO : list) {
            if (rentVO.getRentInDate() == null) {
                rentVO.setStatus("否");
            } else {
                rentVO.setStatus("是");
            }
        }
        return list;
    }

    @Override
    public void addRent(Rent rent) {
        Integer id = rent.getItemId();
        RentGoodsVO rentGoodsVO = rentDao.selectRentGoodById(id);
        Integer count = rentGoodsVO.getAvailableCount();
        if (count > 0) {
            rentDao.downRent(id);
            rentDao.addRent(rent);
        }
    }

    @Override
    public Rent selectRentById(Integer id) {
        return rentDao.selectRentById(id);
    }


    @Override
    public void updateRent(Rent rent) {
        Integer id = rent.getItemId();
        Date rent_in_date = rent.getRentInDate();
        if (rent_in_date == null) {
            rentDao.updateRent(rent);
        } else {
            rentDao.upRent(id);
            rentDao.updateRent(rent);
        }
    }
}
