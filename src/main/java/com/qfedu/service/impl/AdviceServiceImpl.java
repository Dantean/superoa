package com.qfedu.service.impl;

import com.github.pagehelper.PageHelper;
import com.qfedu.dao.AdviceDao;
import com.qfedu.dao.LogDao;
import com.qfedu.dto.AdviceUser;
import com.qfedu.dto.ViewDateObject;
import com.qfedu.entity.Advice;
import com.qfedu.entity.Log;
import com.qfedu.entity.User;
import com.qfedu.service.AdviceService;
import com.qfedu.vo.AdviceEcharts;
import com.qfedu.vo.AdviceVO;
import com.qfedu.vo.EchartsVO;
import com.qfedu.vo.LogVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

@Component
public class AdviceServiceImpl implements AdviceService {

    @Autowired
    private AdviceDao adviceDao;

    @Autowired
    private LogDao logDao;

    @Override
    public List<AdviceVO> selectByUid(User user, String title, String content, Integer page, Integer limit) {
        if (user == null) {
            throw new RuntimeException("用户未登陆");
        }
        AdviceVO adviceVO = new AdviceVO();
        adviceVO.setTitle(title);
        adviceVO.setContent(content);

        // 设置页码和每页显示的记录数，该语句后面一定要紧跟着数据库查询语句
        PageHelper.startPage(page,limit);
        List<AdviceVO> list = adviceDao.selectByUid(adviceVO);
        return list;
    }

    @Override
    public void deleteById(User user, Integer id) {
        if (user == null) {
            throw new RuntimeException("用户未登陆");
        }
        adviceDao.deleteById(id);
    }

    @Override
    public void deleteByIds(User user, String ids) {
        if (user == null) {
            throw new RuntimeException("用户未登陆");
       }

        String[] arr = ids.split(",");
        List<Integer> list = new ArrayList<>();

        for (int i = 0; i < arr.length; i++) {
             list.add(Integer.parseInt(arr[i]));
        }
        adviceDao.deleteByIds(list);

    }


    /**
     * 继承自AdviceService的添加公共的方法
     */
    @Override
    public void addAdvice(Advice advice) {
        adviceDao.addAdvice(advice);
    }

    @Override
    public Advice selectById(User user, Integer id) {
        if (user == null) {
            throw new RuntimeException("用户未登陆");
       }
        Advice advice = adviceDao.selectById(id);
        return advice;
    }

    @Override
    public void updateAdvice(User user, Advice advice) {
        if (user == null) {
            throw new RuntimeException("用户未登陆");
       }
        adviceDao.updateAdvice(advice);
        Log log = new Log();
        log.setUid(user.getId());
        log.setBid(5);
        log.setOid(3);
        Log lo = logDao.selectOneLog(log);
        if (lo != null) {
            lo.setOperateCount(log.getOperateCount() + 1);
            logDao.addLog(lo);
        } else {
            log.setOperateCount(1);
            log.setGid(1);
            log.setDoDate(new Date());
            logDao.addLog(log);
        }
    }

    @Override
    public EchartsVO echarts() {
        EchartsVO echartsVO = new EchartsVO();

        List<ViewDateObject> echarts = adviceDao.echarts();
        Iterator it1 = echarts.iterator();

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        List<String> d = new ArrayList<>();
        List<Integer> s = new ArrayList<>();
        while(it1.hasNext()){
            ViewDateObject vdo =  (ViewDateObject) it1.next();
            String dateString = formatter.format(vdo.getCtime());
            d.add(dateString);
            s.add(vdo.getSums());
            //maps.put(dateString,vdo.getSums());

        }
        echartsVO.setCtime(d);
        echartsVO.setSums(s);
        return echartsVO;
    }

    @Override
    public List<EchartsVO> echartsAdmin() {
        return adviceDao.echartsAdmin();
    }

    @Override
    public AdviceEcharts selectGroupBy() {

        List<AdviceUser> list = adviceDao.selectCount();
        AdviceEcharts aes = new AdviceEcharts();
        aes.setUsers(new ArrayList<>());
        aes.setCounts(new ArrayList<>());

        for (AdviceUser au: list) {
            aes.getUsers().add(au.getUsername());
            aes.getCounts().add(au.getCount());
        }
        return aes;
    }
}
