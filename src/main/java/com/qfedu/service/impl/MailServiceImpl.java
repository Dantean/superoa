package com.qfedu.service.impl;

import com.qfedu.service.MailService;
import com.sun.mail.util.MailSSLSocketFactory;
import org.springframework.stereotype.Service;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.security.GeneralSecurityException;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/9/1
 * Time: 14:10
 * Code introduction:
 */

@Service
public class MailServiceImpl implements MailService {
    @Override
    public void SendMail(String recipient,String title,String content) throws Exception {
        Properties prop = new Properties();
        prop.setProperty("mail.host", "smtp.qq.com");  //设置QQ邮件服务器
        prop.setProperty("mail.transport.protocol", "smtp"); // 邮件发送协议
        prop.setProperty("mail.smtp.auth", "true"); // 需要验证用户名密码

        // 关于QQ邮箱，还要设置SSL加密，加上以下代码即可
        MailSSLSocketFactory sf = new MailSSLSocketFactory();
        sf.setTrustAllHosts(true);
        prop.put("mail.smtp.ssl.enable", "true");
        prop.put("mail.smtp.ssl.socketFactory", sf);

        Session session = Session.getDefaultInstance(prop, new Authenticator() {
            @Override
            public PasswordAuthentication getPasswordAuthentication() {
                //发件人邮件用户名、授权码
                return new PasswordAuthentication("1433652751@qq.com", "cuugitntcgmogiic");
            }
        });
        //开启Session的debug模式，这样就可以查看到程序发送Email的运行状态
        session.setDebug(true);

        //2、通过session得到transport对象
        Transport ts = session.getTransport();

        //3、使用邮箱的用户名和授权码连上邮件服务器
        ts.connect("smtp.qq.com", "1433652751@qq.com", "cuugitntcgmogiic");

        //4、创建邮件

        //创建邮件对象
        MimeMessage message = new MimeMessage(session);

        //指明邮件的发件人
        message.setFrom(new InternetAddress("1433652751@qq.com"));

        //指明邮件的收件人      recipient收件人地址
        message.setRecipient(Message.RecipientType.TO, new InternetAddress(recipient));

        //邮件的标题
        message.setSubject(title);

        //邮件的文本内容
        message.setContent(content, "text/html;charset=gbk");

        //5、发送邮件
        ts.sendMessage(message, message.getAllRecipients());

        ts.close();

    }
}
