package com.qfedu.service.impl;

import com.alibaba.excel.EasyExcel;
import com.github.pagehelper.PageHelper;
import com.qfedu.dao.JobDao;
import com.qfedu.entity.Job;
import com.qfedu.service.JobService;
import com.qfedu.vo.JobEcharts;
import com.qfedu.vo.JobInfoVO;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author HZF
 */
@Service
public class JobServiceImpl implements JobService {

    @Resource
    private JobDao jobDao;

    @Override
    public List<JobInfoVO> selectAllJob(String leadName, String position, int page, int limit) {

        PageHelper.startPage(page, limit);
        List<JobInfoVO> list= jobDao.selectAllJob(leadName, position);

        return list;
    }

    @Override
    public List<Job> selectJobName() {
        return jobDao.selectJobName();
    }

    @Override
    public void addJobType(Job job) {
        jobDao.addJobType(job);
    }

    @Override
    public void deleteJobById(Integer id) {
        int jobId = jobDao.selectJobId(id);
        jobDao.deleteJobType(jobId);
    }

    @Override
    public void deleteLotsJob(String ids) {
        String temp = ids.replace("[","").replace("]","");
        String[] str = temp.split(",");

        for (int i = 0; i < str.length; i++) {
            int jobId = selectJobId(Integer.parseInt(str[i]));
            jobDao.deleteJobType(jobId);
        }
    }

    @Override
    public int selectJobId(Integer id) {
        return jobDao.selectJobId(id);
    }

    @Override
    public JobEcharts selectJobEchartsInfo() {

        List<JobInfoVO> list = jobDao.selectJobEchartsInfo();
        JobEcharts jobEcharts = new JobEcharts();

        jobEcharts.setName(new ArrayList<>());
        jobEcharts.setJobCount(new ArrayList<>());

        for (JobInfoVO jobInfoVO : list) {
            jobEcharts.getName().add(jobInfoVO.getJobName());
            jobEcharts.getJobCount().add(jobInfoVO.getCount());
        }

        return jobEcharts;
    }

    @Override
    public void loadJobChart(HttpServletResponse response) {
        /*1.查询要生成的内容*/
        List<JobInfoVO> list = jobDao.selectJobEchartsInfo();

        /*下载设置响应头,application/octet-stream 二进制 下载*/
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);

        /*设置下载的文件名称*/
        response.setHeader("Content-Disposition","attachment;filename=jobInfo.xlsx");
        try {
            /*将要下载的内容转换为Excel格式，并且写出*/
            EasyExcel.write(response.getOutputStream(), JobInfoVO.class).sheet("job表").doWrite(list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
