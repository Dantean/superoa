package com.qfedu.service.impl;

import com.github.pagehelper.PageHelper;
import com.qfedu.dao.BankAccountDao;
import com.qfedu.entity.BankAccount;
import com.qfedu.service.BankAccountService;
import com.qfedu.vo.BankAccountVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class BankAccountServiceImpl implements BankAccountService {
    @Autowired
    private BankAccountDao bankAccountDao;


//    模糊查询
    @Override
    public List<BankAccountVo> findAll(String name, Date beginTime, Date endTime, Integer page, Integer limit) {

        PageHelper.startPage(page,limit);
        List<BankAccountVo> list = bankAccountDao.selectBankAccount(name, beginTime, endTime);

        return list;
    }

//    添加部门
    @Override
    public void addBankAccount(BankAccount bankAccount) {
        bankAccount.setBalance(Math.random()*1000000 + 1);
        bankAccountDao.addAccount(bankAccount);
    }


//    单行删除
    @Override
    public void deleteBankAccountById(String id) {
        bankAccountDao.deleteAccountById(id);
    }

//  批量删除
    @Override
    public void deleteBankAccountByIds(String id) {
        String temp = id.replace("[","").replace("]","");
        String[] strs = temp.split(",");

        //将字符串数组转换成集合list
        List list = Arrays.asList(strs);

        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }

        bankAccountDao.deleteAccountByIds(list);
    }

 //  修改账户
    @Override
    public void updateBankAccount(BankAccount bankAccount) {
        bankAccountDao.updateAccount(bankAccount);
    }
}
