package com.qfedu.service.impl;

import com.alibaba.excel.EasyExcel;
import com.qfedu.dao.FileUploadDao;
import com.qfedu.entity.Advice;
import com.qfedu.service.FileUploadService;
import com.qfedu.vo.AdviceVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Component
public class FileUploadServiceImpl implements FileUploadService {

    @Autowired
    private FileUploadDao fileUploadDao;

    @Override
    public int batch(MultipartFile file) throws IOException {
        // 验证非空
        if (!file.isEmpty()) {
            // 解析结果 同步解析
            List<Advice> list = EasyExcel.read(file.getInputStream()).sheet().head(Advice.class).doReadSync();
            if (list!= null && list.size() > 0) {
                System.out.println(list.size());
                // 操作数据库
                for (Advice advice : list) {
                    System.out.println(advice);
                }
                return fileUploadDao.saveBatch(list);
            }

        }
        return -1;
    }

    @Override
    public void download(Integer num, HttpServletResponse response) throws IOException {
        // 1.查询要生成的内容
        List<Advice> list = fileUploadDao.selectNum(num);
        //2.设置下载 需要设置响应消息头
        //application/octet-stream 二进制 下载
        response.setContentType(MediaType.APPLICATION_OCTET_STREAM_VALUE);
        //设置下载的文件名称
        response.setHeader("Content-Disposition","attachment;filename=advice.xlsx");
        // 将要下载的内容转换为Excel格式，并写出
        EasyExcel.write(response.getOutputStream(), Advice.class).sheet("系统自动导出" + list.size()).doWrite(list);

    }
}
