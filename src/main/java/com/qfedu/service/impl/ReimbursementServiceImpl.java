package com.qfedu.service.impl;

import com.github.pagehelper.PageHelper;
import com.qfedu.dao.ReimbursementDao;
import com.qfedu.entity.Reimbursement;
import com.qfedu.service.ReimbursementService;
import com.qfedu.vo.ReimbursementVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class ReimbursementServiceImpl implements ReimbursementService {
    @Autowired
    private ReimbursementDao reimbursementDao;

    @Override
    public List<ReimbursementVo> selectAllReimbursementInfo(String name, Date beginTime, Date endTime, Integer page, Integer limit) {

        PageHelper.startPage(page,limit);
        List<ReimbursementVo> list = reimbursementDao.selectAllReimbursement(name, beginTime, endTime);

        return list;
    }

    @Override
    public void addReimbursementInfo(Reimbursement reimbursement) {
        double v = Math.random() * 30;
        String str = (v <10||v==10)? "待审核": ((v<20||v==20)?"未通过":"已报销");
        reimbursement.setStatus(str);
        reimbursementDao.addReimbursement(reimbursement);
    }

    @Override
    public void deleteReimbursementInfoById(String id) {
        reimbursementDao.deleteReimbursementById(id);
    }

    @Override
    public void deleteReimbursementInfoByIds(String id) {
        String temp = id.replace("[","").replace("]","");
        String[] strs = temp.split(",");

        //将字符串数组转换成集合list
        List list = Arrays.asList(strs);


        for (int i = 0; i < list.size(); i++) {
            System.out.println(list.get(i));
        }
        reimbursementDao.deleteReimbursementByIds(list);
    }
}
