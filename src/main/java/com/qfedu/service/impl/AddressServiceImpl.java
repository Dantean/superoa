package com.qfedu.service.impl;

import com.qfedu.dao.AddressDao;
import com.qfedu.service.AddressService;
import com.qfedu.vo.AddressVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AddressServiceImpl implements AddressService {

    @Autowired
    private AddressDao addressDao;

    @Override
    public List<AddressVO> selectProvinces() {
        List<AddressVO> addressVOS = addressDao.selectProvinces();
        return addressVOS != null ? addressVOS : new ArrayList<>();
    }

    @Override
    public List<AddressVO> selectCities(String provinceCode) {
        List<AddressVO> addressVOS = addressDao.selectCities(provinceCode);
        return addressVOS != null ? addressVOS : new ArrayList<>();
    }

    @Override
    public List<AddressVO> selectAreas(String cityCode) {
        List<AddressVO> addressVOS = addressDao.selectAreas(cityCode);
        return addressVOS != null ? addressVOS : new ArrayList<>();
    }
}
