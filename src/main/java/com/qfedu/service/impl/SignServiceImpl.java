package com.qfedu.service.impl;

import com.qfedu.dao.SignDao;
import com.qfedu.entity.Sign;
import com.qfedu.entity.User;
import com.qfedu.service.SignService;
import com.qfedu.utils.StrUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpSession;
import java.util.Date;

@Service
public class SignServiceImpl implements SignService {
    @Autowired
    SignDao signDao;

    @Override
    public String moSign(Sign sign) {
        Date date = new Date();
        sign.setCreateTime(date);

        String flag = "";

        if (date.getHours() > 8) {
            flag = "迟到";
            sign.setFlag(flag);
        } else {
            flag = "正常";
            sign.setFlag(flag);
        }
        signDao.moSign(sign);

        return flag;
    }
}
