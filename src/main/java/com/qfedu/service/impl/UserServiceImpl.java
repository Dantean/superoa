package com.qfedu.service.impl;

import com.github.pagehelper.PageHelper;
import com.qfedu.dao.LogDao;
import com.qfedu.dao.UserDao;
import com.qfedu.dto.Userdto;
import com.qfedu.entity.User;
import com.qfedu.service.UserService;
import com.qfedu.vo.LoginLogVO;
import com.qfedu.vo.UserVO;
import com.qfedu.utils.OSSUtils;
import com.qfedu.vo.UserVO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.sound.midi.Soundbank;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private LogDao logDao;


    //登录
    @Override
    public User findByName(String loginname, String password) {
        User users = userDao.findByName(loginname);
        if (users == null) {
            throw new RuntimeException("亲,账号不存在,请先注册哦");
        }
        if (!users.getPassword().equals(password)) {
            throw new RuntimeException("亲，密码错误，请仔细核对哦");
        }
        if (users.getStatus() == 1) {
           throw new RuntimeException("亲,您的账号已在异地登录哦");
        }
        LoginLogVO loginLogVO = logDao.selectOneLoginLog(users.getId());
        if (loginLogVO != null) {
            Integer loginCount = loginLogVO.getLoginCount();
            loginLogVO.setLoginCount(loginCount + 1);
            System.out.println(loginLogVO.toString());
            logDao.addLoginLog(loginLogVO);
        } else {
            LoginLogVO lv = new LoginLogVO();
            lv.setUid(users.getId());
            lv.setLoginCount(1);
            lv.setLoginTime(new Date());
            logDao.addLoginLog(lv);
        }
        return users;
    }

    //    添加用户  注册
    @Override
    public void addUser(User user) {

        // TODO 暂时给定status一个值
        if (user.getStatus() == null) {
            user.setStatus(1);
        }

        try {
            userDao.addUser(user);
        } catch (Exception e) {
            throw new RuntimeException("添加失败");
        }
    }

    //    删除用户
    @Override
    public void deleteUser(String ids) {

        ArrayList<Integer> list = new ArrayList<>();
        String[] target = ids.split(",");
        for (int i = 0; i < target.length; i++) {
            list.add(Integer.parseInt(target[i]));
        }
        System.out.println(list + "AAA");
//        List<String> list = Arrays.asList(target);

            userDao.deleteUser(list);

    }

    //    更新用户
    @Override
    public void updateUser(User user) {
        try {
            userDao.updateUser(user);
        } catch (Exception e) {
            throw new RuntimeException("更新失败");
        }
    }

    @Override
    public void updateUser1(Integer id, Integer status) {
        User user = new User();
        user.setId(id);
        user.setStatus(status);
        userDao.updateUser(user);
    }

    //    展示用户
    @Override
    public List<User> selectUser(Integer page, Integer limit) {
        PageHelper.startPage(page, limit);
        return userDao.selectUser();
    }

//    查询一个
    @Override
    public User selectUserById(Integer id) {
        return userDao.selectUserById(id);
    }


//    返回管理员人数和类型集合
    @Override
    public UserVO userChart() {
        List<Userdto> list = userDao.userChart();

        UserVO userVO = new UserVO();
        userVO.setVOstatus(new ArrayList<>());
        userVO.setStatusCount(new ArrayList<>());

        for (Userdto userdto : list) {
            userVO.getVOstatus().add(userdto.getUser());
            userVO.getStatusCount().add(userdto.getCount());
        }
        return userVO;
    }

    @Override
    public String upUserAvatar(MultipartFile file, User user){

        String s = null;
        try {
            s = OSSUtils.upLoad(file, user);
        } catch (IOException e) {
            e.printStackTrace();
        }

        user.setImgPath(s);
        userDao.updateUser(user);
        System.out.println("更新成功");

        return s;
    }

    @Override
    public void addOneUser(String loginname, String username, String password, String queryPassword) {
        User byName = userDao.findByName(loginname);
        if (byName != null) {
            throw new RuntimeException("亲，账号已存在，请换个可爱的名字哦");
        }
        if (!password.equals(queryPassword)) {
            throw new RuntimeException("亲，两次输入密码不一致，请仔细检查下哦");
        }

        User user = new User();
        user.setStatus(0);
        user.setLoginname(loginname);
        user.setUsername(username);
        user.setCreateDate(new Date());
        user.setPassword(password);
        userDao.addUser(user);
    }


}