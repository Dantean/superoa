package com.qfedu.service;

import com.qfedu.entity.Job;
import com.qfedu.vo.JobEcharts;
import com.qfedu.vo.JobInfoVO;

import javax.servlet.http.HttpServletResponse;
import java.util.List;

/**
 * @author HZF
 */
public interface JobService {
    /**
     * 查询所有员工的职位信息进行展示
     * @param page 页数
     * @param limit 每页展示的数据量
     * @param leadName 领导名字
     * @param position 所属部门名字
     * @return 查找到的数据
     */
    public List<JobInfoVO> selectAllJob(String leadName, String position, int page, int limit);

    /**
     * 查询数据库中所有职称
     * @return 返回查找到的数据
     */
    public List<Job> selectJobName();

    /**
     * 添加一个职位类型到职位数据库中
     * @param job 前端页面传过来的职位类型和描述
     */
    public void addJobType(Job job);

    /**
     * 通过id去删除某个职位
     * @param id 指定id
     */
    public void deleteJobById(Integer id);

    /**
     * 通过id去删除job表中的字段
     * @param ids 多个id
     */
    public void deleteLotsJob(String ids);

    /**
     * 通过员工表查出所对应的职位id
     * @param id 员工id
     * @return 职位id
     */
    public int selectJobId(Integer id);

    /**
     * 查找job表中的职位名字和信息
     * @return 查找到的数据
     */
    public JobEcharts selectJobEchartsInfo();

    /**
     * 下载html页面的图表中的数据
     * @param response 响应头
     */
    public void loadJobChart(HttpServletResponse response);
}
