package com.qfedu.service;



import com.qfedu.entity.Advice;
import com.qfedu.entity.User;
import com.qfedu.vo.AdviceEcharts;
import com.qfedu.vo.AdviceVO;
import com.qfedu.vo.EchartsVO;

import java.util.List;

public interface AdviceService {
    // 模糊查询 + 分页
    List<AdviceVO> selectByUid(User user, String title, String content, Integer page, Integer limit);
    // 通过id删除指定数据
    void deleteById(User user, Integer id);
    // 批量删除指定数据
    void deleteByIds(User user, String ids);

    /**
     * 添加公告
     * @param advice 公告对象
     */
    public void addAdvice(Advice advice);

    Advice selectById(User user, Integer id);

    void updateAdvice(User user, Advice advice);

    EchartsVO echarts();

    AdviceEcharts selectGroupBy();

    List<EchartsVO> echartsAdmin();

}
