package com.qfedu.service;

import com.qfedu.vo.AddressVO;

import java.util.List;

public interface AddressService {
    List<AddressVO> selectProvinces();
    List<AddressVO> selectCities(String provinceCode);
    List<AddressVO> selectAreas(String cityCode);
}
