package com.qfedu.service;

import com.qfedu.entity.AskLeave;
import com.qfedu.vo.AskLeaveVO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface AskLeaveService {

    int add(AskLeave askLeave);

    int update(AskLeave askLeave);

    List<AskLeaveVO> select(AskLeave askLeave, Integer page, Integer limit);

    int delete(AskLeave askLeave);

    int deleteByIds(String ids);

    List<AskLeaveVO> selectAllType();

}
