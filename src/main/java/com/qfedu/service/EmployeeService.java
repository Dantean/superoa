package com.qfedu.service;

import com.qfedu.entity.Employee;
import com.qfedu.vo.EmployeeVO;

import java.util.List;
/**
 * @author: Wufeng
 * @date: 2020/9/1 10:48
 * @description:
 */
public interface EmployeeService {
    /*
    *
    * 添加员工
    *
    * @param employee 员工
    * */
    public void addEmployee(Employee employee);
    public void update(Employee employee);

    List<EmployeeVO> selectFuzzy(Employee employee, Integer page, Integer limit);

    int deleteByIds(String str);


}
