package com.qfedu.service;

import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public interface FileUploadService {

    int batch(MultipartFile file) throws IOException;

    void download(Integer num, HttpServletResponse response) throws IOException;
}
