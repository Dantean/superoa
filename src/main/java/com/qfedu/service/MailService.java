package com.qfedu.service;

import java.security.GeneralSecurityException;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/9/1
 * Time: 14:08
 * Code introduction:
 */
public interface MailService {
    public void SendMail(String recipient,String title,String content) throws Exception;

}
