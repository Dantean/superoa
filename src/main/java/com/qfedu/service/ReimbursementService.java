package com.qfedu.service;

import com.qfedu.entity.Reimbursement;
import com.qfedu.vo.ReimbursementVo;

import java.util.Date;
import java.util.List;

public interface ReimbursementService {
    List<ReimbursementVo> selectAllReimbursementInfo(String name, Date beginTime, Date endTime, Integer page, Integer limit);

    void addReimbursementInfo(Reimbursement reimbursement);

    void deleteReimbursementInfoById(String id);

    void deleteReimbursementInfoByIds(String id);
}
