package com.qfedu.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.CellExtra;
import com.alibaba.excel.read.listener.ReadListener;
import com.aliyuncs.OssAcsRequest;
import com.qfedu.entity.Advice;
import com.qfedu.entity.User;
import com.qfedu.utils.OSSUtils;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by IntelliJ IDEA.
 * User: JingWang
 * Date: 2020/9/4
 * Time: 14:40
 * Code introduction:
 */


public class EasyExcelMain {
    //public static void main(String[] args) {
    //    //生成excel
    //    //List<Advice> list1 = new ArrayList<>();
    //    //
    //    //for (int i = 1; i < 1000; i++) {
    //    //    Calendar calendar = Calendar.getInstance();
    //    //    Advice advice = new Advice();
    //    //    calendar.add(Calendar.YEAR,-i%20);
    //    //    advice.setTitle("公告标题"+i);
    //    //    advice.setContent("公告内容"+i);
    //    //    advice.setUid(i);
    //    //    advice.setCreateDate(calendar.getTime());
    //    //    advice.setUid((int)Math.random()*10);
    //    //    advice.setId(i);
    //    //    list1.add(advice);
    //    //    advice=null;
    //    //}
    //    //write():1,指定的路径和文件名  2,指定导入每行的对象
    //    //sheet():设置excel表格sheet的名称
    //    //doWrite():要写出的数据集合
    //
    //    //EasyExcel.write("advices.xlsx",Advice.class).sheet("第一页").doWrite(list1);
    //
    //
    //    //解析excel
    //    //MyReadListener mr = new MyReadListener();
    //    //EasyExcel.read("advices.xlsx",Advice.class,mr).sheet("第一页").doRead();
    //    //System.out.println(mr.getAdvices());
    //
    //
    //    File file = new File("E:/VirtualPath/2.jpg" );
    //    User user = new User();
    //    user.setId(1);
    //    String s = null;
    //    try {
    //        s = OSSUtils.upLoad(file, user);
    //    } catch (FileNotFoundException e) {
    //        e.printStackTrace();
    //    }
    //    System.out.println(s.toString());
    //}
    static class MyReadListener implements ReadListener<Advice>{
        private List<Advice> advices =new ArrayList<>();

        public List<Advice> getAdvices() {
            return advices;
        }
        //出现异常信息
        @Override
        public void onException(Exception e, AnalysisContext analysisContext) throws Exception {
            System.out.println(e.getMessage());
        }
        //获取头部信息
        @Override
        public void invokeHead(Map<Integer, CellData> map, AnalysisContext analysisContext) {
            System.out.println("map"+map);
        }
        //读取内容
        @Override
        public void invoke(Advice advice, AnalysisContext analysisContext) {
            advices.add(advice);
        }
        //获取额外信息
        @Override
        public void extra(CellExtra cellExtra, AnalysisContext analysisContext) {

        }
        //分析完成
        @Override
        public void doAfterAllAnalysed(AnalysisContext analysisContext) {

        }
        //是否继续读取
        @Override
        public boolean hasNext(AnalysisContext analysisContext) {
            return true;
        }
    }
}
