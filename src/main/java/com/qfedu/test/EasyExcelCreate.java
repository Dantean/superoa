package com.qfedu.test;


import com.alibaba.excel.EasyExcel;
import com.qfedu.entity.Advice;
import com.qfedu.vo.AdviceVO;

import java.util.ArrayList;
import java.util.Calendar;

/**
 * 批量生成Excel文档
 */
public class EasyExcelCreate {
    public static void main(String[] args) {
        ArrayList<Advice> list = new ArrayList<>();

        for (int i = 500; i < 1000; i++) {
            Advice advice = new Advice();
            // 获取当前时间对象
            Calendar calendar = Calendar.getInstance();
            System.out.println(Calendar.getInstance());
            calendar.add(Calendar.YEAR,-i%20);
            advice.setTitle("给侯哥的第" + i + "封情书");
            advice.setContent("我喜欢侯哥的第" + i + "天");
            advice.setCreateDate(calendar.getTime());
            advice.setUid((int)(Math.random()*7 + 1));

            list.add(advice);
        }
        /**
         * write: 1. 文件的路径和名称 （默认项目根目录） 2. 要生成数据的Class对象
         * sheet：设置Sheet的名称
         * dowrite：写出内容  传递要写出的集合
         */
        EasyExcel.write("advice1.xlsx",AdviceVO.class).sheet("第一个").doWrite(list);

    }

}
