package com.qfedu.test;

import com.alibaba.excel.EasyExcel;
import com.alibaba.excel.context.AnalysisContext;
import com.alibaba.excel.metadata.CellData;
import com.alibaba.excel.metadata.CellExtra;
import com.alibaba.excel.read.listener.ReadListener;
import com.qfedu.vo.AdviceVO;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * 解析excel文件
 *
 */
public class EasyExcelParse {
    public static void main(String[] args) {

        // 解析excel文件 返回值为list集合
        List<AdviceVO> list = EasyExcel.read("advice.xlsx").sheet().head(AdviceVO.class).doReadSync();
        for (AdviceVO adviceVO : list) {
            System.out.println(adviceVO);
        }
        // 解析excel文件
//        MyReadListener myReadListener = new MyReadListener();
//        EasyExcel.read("advice.xlsx",AdviceVO.class, myReadListener).sheet("第一个").doRead();
//        System.out.println(myReadListener.getList());
    }

//    static class MyReadListener implements ReadListener<AdviceVO>{
//        // 存储读取的内容
//        private List<AdviceVO> list = new ArrayList<>();
//
//        public List<AdviceVO> getList() {
//            return list;
//        }
//
//        public void setList(List<AdviceVO> list) {
//            this.list = list;
//        }
//
//        // 出现异常信息
//        @Override
//        public void onException(Exception e, AnalysisContext analysisContext) throws Exception {
//            System.out.println(e.getMessage());
//        }
//
//        // 获取头部信息
//        @Override
//        public void invokeHead(Map<Integer, CellData> map, AnalysisContext analysisContext) {
//
//        }
//        // 读取内容
//        @Override
//        public void invoke(AdviceVO adviceVO, AnalysisContext analysisContext) {
//            list.add(adviceVO);
//        }
//        // 获取额外信息
//        @Override
//        public void extra(CellExtra cellExtra, AnalysisContext analysisContext) {
//
//        }
//        // 分析完成
//        @Override
//        public void doAfterAllAnalysed(AnalysisContext analysisContext) {
//
//        }
//        // 是否继续读取
//        @Override
//        public boolean hasNext(AnalysisContext analysisContext) {
//            return true;
//        }
//    }
}
