package com.qfedu.vo;

import java.util.List;
import java.util.Map;

public class AdviceEcharts {
    private List<String> users;
    private List<Integer> counts;

    public List<String> getUsers() {
        return users;
    }

    public void setUsers(List<String> users) {
        this.users = users;
    }

    public List<Integer> getCounts() {
        return counts;
    }

    public void setCounts(List<Integer> counts) {
        this.counts = counts;
    }
}
