package com.qfedu.interceptor;

import com.qfedu.entity.User;
import com.qfedu.utils.StrUtils;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

// 实现登录拦截器

public class LoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        String requestURI = request.getRequestURI();
        System.out.println(requestURI);

        User user = (User)request.getSession().getAttribute(StrUtils.LOGIN_USER);

        if (user == null) {
            // 未登录状态 分为 ajax请求(.do) 和静态页面请求(html  js)
            // ajax 请求会有特殊的请求头X-Requested-With
            String header = request.getHeader("X-Requested-With");
            if (header != null && header.equals("XMLHttpRequest")) {
                response.getWriter().write("{\"code\":0,\"info\":\"未登录\"}");    // 转义字符
            } else {
                //  如果不是ajax请求 比如index.html
                response.sendRedirect(request.getContextPath() + "/after/user/login.html");
            }


            return false;
        } else {
            // user不为空 说明为登录成功状态 直接放行
            return true;
        }

    }
}
